<?php

echo 'Sintaxis variables php';

$A=1;
$a=2.5;
$_a=2/5;
$b="5";
echo "$a<br/>";
echo "$A<br/>";
echo "$_a<br/>";
echo "$a+$_a . <br/>";
var_dump(is_int($A));
echo '<br/>';
var_dump(is_float($a));
echo '<br/>';
$x=2;
$y=5;
$z="5";
echo $x + $y.'<br/>';
echo $x - $y.'<br/>';
echo $x * $y.'<br/>';
echo $x / $y.'<br/>';
echo $x % $y.'<br/>';
echo $x ** $y.'<br/>';

echo 'Operadores Comparación';
if ($y == $z) {
    echo '<br/>son iguales<br/>';
} else {
    echo '<br/>no son iguales<br/>';
}

if ($y === $z) {
    echo '<br/>son iguales<br/>';
} else {
    echo '<br/>no son iguales<br/>';
}

$x= $x+$y;
$x+=$y;

$x++;
++$x;
?>