<?php

echo 'archivo php';
$str = 'Hola Mundo!';
echo "$str<br/>";
echo $str[12]."<br/>";

echo 'Longitud '. strlen($str) . '<br/>';
echo 'Número palabras '. str_word_count($str) . '<br/>';
echo 'Inverso ' . strrev($str) . '<br/>';
echo 'Posición ' . strpos($str,'Mundo') . '<br/>';
echo 'Reemplazar ' . str_replace('Mundo','PLaneta',$str) . '<br/>';

$pattern="/Mundo/";
echo preg_match($pattern, $str);
echo preg_match_all($pattern, $str);
echo preg_replace($pattern, "planeta", $str);

?>